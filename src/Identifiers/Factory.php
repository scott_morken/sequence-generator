<?php


namespace Smorken\SeqGen\Identifiers;


use Smorken\SeqGen\Contracts\Identifier;
use Smorken\SeqGen\Contracts\IdentifierFactory;

class Factory implements IdentifierFactory
{

    /**
     * @var \Smorken\SeqGen\Contracts\Identifier
     */
    protected $system_identifier;

    /**
     * @var \Smorken\SeqGen\Contracts\Identifier
     */
    protected $user_identifier;

    public function __construct(Identifier $system_identifier, Identifier $user_identifier)
    {
        $this->setSystemIdentifierCreator($system_identifier);
        $this->setUserIdentifierCreator($user_identifier);
    }

    public function create($system_identifier, $user_provided_identifier = null): int
    {
        return ($this->getSystemIdentifierCreator()->create($system_identifier) + $this->getUserIdentifierCreator()
                                                                                       ->create($user_provided_identifier));
    }

    public function getSystemIdentifierCreator(): Identifier
    {
        return $this->system_identifier;
    }

    public function getUserIdentifierCreator(): Identifier
    {
        return $this->user_identifier;
    }

    public function setSystemIdentifierCreator(Identifier $creator): void
    {
        $this->system_identifier = $creator;
    }

    public function setUserIdentifierCreator(Identifier $creator): void
    {
        $this->user_identifier = $creator;
    }
}
