<?php


namespace Smorken\SeqGen\Identifiers;


use Smorken\SeqGen\Contracts\Identifier;

class IntVal extends Base implements Identifier
{

    protected function identifierToInt($identifier): int
    {
        return intval($identifier);
    }
}
