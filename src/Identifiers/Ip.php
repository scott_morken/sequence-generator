<?php


namespace Smorken\SeqGen\Identifiers;


use Smorken\SeqGen\Contracts\Identifier;

class Ip extends Base implements Identifier
{

    protected function identifierToInt($identifier): int
    {
        if (filter_var($identifier, FILTER_VALIDATE_IP)) {
            return (strpos($identifier, ':') === false ? ip2long($identifier) : $this->ip2longV6($identifier));
        }
        throw new Exception("$identifier is not a valid IP.");
    }

    protected function ip2longV6($ip)
    {
        $ip_n = inet_pton($ip);
        $bin = '';
        for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
            $bin = sprintf('%08b', ord($ip_n[$bit])).$bin;
        }
        $dec = '0';
        for ($i = 0; $i < strlen($bin); $i++) {
            $dec = bcmul($dec, '2', 0);
            $dec = bcadd($dec, $bin[$i], 0);
        }
        return $dec;
    }
}
