<?php

namespace Smorken\SeqGen\Identifiers;

use Smorken\SeqGen\Contracts\Identifier;

abstract class Base implements Identifier
{

    abstract protected function identifierToInt($identifier): int;

    public function create($identifier): int
    {
        if (!is_null($identifier)) {
            return $this->identifierToInt($identifier);
        }
        return 0;
    }
}
