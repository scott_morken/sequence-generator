<?php


namespace Smorken\SeqGen\Identifiers;


class Zero extends Base
{

    protected function identifierToInt($identifier): int
    {
        return 0;
    }
}
