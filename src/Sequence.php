<?php


namespace Smorken\SeqGen;


class Sequence implements \Smorken\SeqGen\Contracts\Sequence
{

    /**
     * @var int
     */
    protected $max = 255;

    public function next(?int $last): int
    {
        $next = is_null($last) ? 0 : $last + 1;
        if (!$this->hasNext($next)) {
            $next = 0;
        }
        return $next;
    }

    public function setMax(int $max): void
    {
        $this->max = $max;
    }

    public function hasNext(int $value): bool
    {
        return $value <= $this->max;
    }
}
