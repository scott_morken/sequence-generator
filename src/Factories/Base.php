<?php


namespace Smorken\SeqGen\Factories;


use Psr\SimpleCache\CacheInterface;
use Smorken\SeqGen\Contracts\Factory;
use Smorken\SeqGen\Contracts\IdentifierFactory;
use Smorken\SeqGen\Contracts\Sequence;
use Smorken\SeqGen\Contracts\Timestamp;
use Smorken\SeqGen\Exception;

abstract class Base implements Factory
{

    /**
     * @var \Psr\SimpleCache\CacheInterface
     */
    protected $cache;

    protected $cache_interval = '+2 seconds';

    /**
     * @var IdentifierFactory
     */
    protected $identifier;

    /**
     * @var Sequence
     */
    protected $sequence;

    /**
     * @var Timestamp
     */
    protected $timestamp;

    public function __construct(
        CacheInterface $cache,
        IdentifierFactory $identifier,
        Sequence $sequence,
        Timestamp $timestamp
    ) {
        $this->setCache($cache);
        $this->setIdentifierFactory($identifier);
        $this->setSequence($sequence);
        $this->setTimestamp($timestamp);
    }

    public function create($system_identifier, $user_provided_identifier = null): int
    {
        $identifier = $this->getIdentifier($this->identifier->create($system_identifier, $user_provided_identifier));
        $timestamp = $this->timestamp->create();
        $last_sequence = $this->getFromCache($identifier, 'sequence');
        $last_timestamp = $this->getFromCache($identifier, 'timestamp');
        $sequence = 0;
        if (!$this->isNewTick($identifier, $timestamp, $last_timestamp)) {
            $sequence = $this->sequence->next($last_sequence);
            if ($this->isSequenceExhausted($last_sequence)) {
                $this->timestamp->wait();
                $timestamp = $this->timestamp->create();
                $sequence = 0;
            }
        }
        $this->setInCache($identifier, 'timestamp', $timestamp);
        $this->setInCache($identifier, 'sequence', $sequence);
        $id = $this->getTimestamp($timestamp) << (static::BITS_IDENTIFIER + static::BITS_SEQUENCE);
        $id |= $identifier << static::BITS_SEQUENCE;
        $id |= $this->getSequence($sequence);
        return $id;
    }

    public function getMaxTimestamp(): int
    {
        return $this->timestamp->getEpoch() + $this->getMaxFromBits(static::BITS_TIMESTAMP);
    }

    public function setCache(CacheInterface $cache): Factory
    {
        $this->cache = $cache;
        return $this;
    }

    public function setCacheInterval(string $interval): Factory
    {
        $this->cache_interval = $interval;
        return $this;
    }

    public function setIdentifierFactory(IdentifierFactory $identifierFactory): Factory
    {
        $this->identifier = $identifierFactory;
        return $this;
    }

    public function split(int $id): array
    {
        $parts = [
            'timestamp' => $this->extractBits($id, static::BITS_IDENTIFIER + static::BITS_SEQUENCE,
                static::BITS_TIMESTAMP),
            'identifier' => $this->extractBits($id, static::BITS_SEQUENCE, static::BITS_IDENTIFIER),
            'sequence' => $this->extractBits($id, 0, static::BITS_SEQUENCE),
            'original' => $id,
        ];
        return $parts;
    }

    protected function applyMask(int $check, int $bits, $zero_allowed = false): int
    {
        if ($check === 0 && $zero_allowed === false) {
            throw new Exception("Value cannot be zero.");
        }
        $mask = $this->getMaxFromBits($bits);
        if ($check > $mask) {
            throw new Exception("Value [$check] must be less than $mask.");
        }
        return $check & $mask;
    }

    protected function extractBits(int $value, int $right_shift, int $bits): int
    {
        $v = $value >> $right_shift;
        return (((1 << $bits) - 1) & $v);
    }

    protected function getFromCache(int $identifier, string $suffix): ?int
    {
        return $this->cache->get($this->getKey($identifier, $suffix), null);
    }

    protected function getIdentifier(int $identifier): int
    {
        $divisor = static::BITS_IDENTIFIER / 4;
        $hash = md5($identifier);
        $inthash = intval(substr($hash, 0, $divisor), 16);
        return $this->applyMask($inthash, static::BITS_IDENTIFIER);
    }

    protected function getKey(int $identifier, string $suffix): string
    {
        return sprintf('SQ-%s-%d-%s', class_basename($this), $identifier, $suffix);
    }

    protected function getMaxFromBits(int $bits, int $subtract = 1): int
    {
        return (2 ** $bits - $subtract);
    }

    protected function getSequence(int $sequence): int
    {
        return $this->applyMask($sequence, static::BITS_SEQUENCE, true);
    }

    public function setSequence(Sequence $sequence): Factory
    {
        $this->sequence = $sequence;
        $this->sequence->setMax($this->getMaxFromBits(static::BITS_SEQUENCE));
        return $this;
    }

    protected function getTimestamp(int $timestamp): int
    {
        return $this->applyMask($timestamp, static::BITS_TIMESTAMP);
    }

    public function setTimestamp(Timestamp $timestamp): Factory
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    protected function isNewTick(int $identifier, int $timestamp, ?int $last_timestamp): bool
    {
        if (is_null($last_timestamp)) {
            $this->setInCache($identifier, 'timestamp', $timestamp);
        }
        if ($last_timestamp && $last_timestamp === $timestamp) {
            return false;
        }
        if ($last_timestamp && $timestamp < $last_timestamp) {
            throw new Exception('Clock is moving backwards, cannot continue.');
        }
        return true;
    }

    /**
     * Current second has exhausted possible sequence numbers for the identifier
     *
     * @param  int|null  $last_sequence
     * @return bool
     */
    protected function isSequenceExhausted(?int $last_sequence): bool
    {
        if (is_null($last_sequence)) {
            return false;
        }
        return $this->sequence->hasNext($last_sequence + 1) === false;
    }

    protected function setInCache(int $identifier, string $suffix, int $value): void
    {
        $this->cache->set($this->getKey($identifier, $suffix), $value,
            \DateInterval::createFromDateString($this->cache_interval));
    }
}
