<?php


namespace Smorken\SeqGen\Factories;


use Smorken\SeqGen\Contracts\Factory;

class F64 extends Base implements Factory
{

    const BITS_IDENTIFIER = 24;

    const BITS_SEQUENCE = 8;

    const BITS_TIMESTAMP = 32;
}
