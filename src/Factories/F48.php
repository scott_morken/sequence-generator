<?php


namespace Smorken\SeqGen\Factories;


use Smorken\SeqGen\Contracts\Factory;

class F48 extends Base implements Factory
{
    const BITS_IDENTIFIER = 12;

    const BITS_SEQUENCE = 6;

    const BITS_TIMESTAMP = 30;
}
