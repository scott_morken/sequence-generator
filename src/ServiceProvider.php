<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/17
 * Time: 7:37 AM
 */

namespace Smorken\SeqGen;

use Illuminate\Contracts\Cache\Repository;
use Smorken\SeqGen\Contracts\Factory;
use Smorken\SeqGen\Identifiers\IntVal;
use Smorken\SeqGen\Identifiers\Ip;

/**
 * Class ServiceProvider
 *
 * @package Smorken\SeqGen
 * @codeCoverageIgnore
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
    }

    public function register()
    {
        $this->app->bind(
            Factory::class,
            function ($app) {
                $bits = $app['config']->get('seqgen.factory_bits', 64);
                $cls = $this->getFactoryClass($bits);
                $c = $app[Repository::class];
                $if = new \Smorken\SeqGen\Identifiers\Factory(new Ip(), new IntVal());
                $s = new Sequence();
                $t = new Timestamp($app['config']->get('seqgen.default_epoch', '2020-01-01 00:00:00'));
                return new $cls($c, $if, $s, $t);
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'seqgen');
        $this->publishes([$config => config_path('seqgen.php')], 'config');
    }

    protected function getFactoryClass(int $bits)
    {
        return sprintf('\Smorken\SeqGen\Factories\F%d', $bits);
    }
}
