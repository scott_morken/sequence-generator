<?php


namespace Smorken\SeqGen\Contracts;


interface Timestamp
{

    const BITS = 41;

    /**
     * @return int
     */
    public function create(): int;

    /**
     * @return int
     */
    public function getEpoch(): int;

    /**
     * @param  string  $format
     */
    public function setDateTimeFormat(string $format): void;

    /**
     * @param  \DateTime|string  $dateTime
     */
    public function setEpoch($dateTime): void;

    /**
     * Block and wait for next millisecond
     */
    public function wait(): void;
}
