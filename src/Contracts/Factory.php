<?php


namespace Smorken\SeqGen\Contracts;


use Psr\SimpleCache\CacheInterface;

interface Factory
{

    public function create($system_identifier, $user_provided_identifier = null): int;

    public function getMaxTimestamp(): int;

    public function setCache(CacheInterface $cache): self;

    public function setCacheInterval(string $interval): self;

    public function setIdentifierFactory(IdentifierFactory $identifierFactory): self;

    public function setSequence(Sequence $sequence): self;

    public function setTimestamp(Timestamp $timestamp): self;

    public function split(int $id): array;
}
