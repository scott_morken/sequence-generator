<?php

namespace Smorken\SeqGen\Contracts;


interface IdentifierFactory
{

    /**
     * @param  mixed  $system_identifier
     * @param  mixed  $user_provided_identifier
     * @return int
     */
    public function create($system_identifier, $user_provided_identifier = null): int;

    public function getSystemIdentifierCreator(): Identifier;

    public function getUserIdentifierCreator(): Identifier;

    public function setSystemIdentifierCreator(Identifier $creator): void;

    public function setUserIdentifierCreator(Identifier $creator): void;
}
