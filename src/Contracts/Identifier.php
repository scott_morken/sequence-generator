<?php
namespace Smorken\SeqGen\Contracts;

interface Identifier
{

    /**
     * @param  mixed  $identifier
     * @return int
     */
    public function create($identifier): int;
}
