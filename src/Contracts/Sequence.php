<?php


namespace Smorken\SeqGen\Contracts;


interface Sequence
{

    public function hasNext(int $value): bool;

    public function next(?int $last): int;

    public function setMax(int $max): void;
}
