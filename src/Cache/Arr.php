<?php


namespace Smorken\SeqGen\Cache;


use Psr\SimpleCache\CacheInterface;

/**
 * Class Arr
 *
 * @package Smorken\SeqGen\Cache
 * @codeCoverageIgnore
 *
 * Don't use in production. Stub for testing.  There are
 * real packages available for caching!
 */
class Arr implements CacheInterface
{

    protected $items = [];

    public function clear()
    {
        $this->items = [];
    }

    public function delete($key)
    {
        unset($this->items[$key]);
    }

    public function deleteMultiple($keys)
    {
        foreach ($keys as $key) {
            $this->delete($key);
        }
    }

    public function get($key, $default = null)
    {
        $curr = $this->items[$key]['value'] ?? null;
        if ($curr && $this->isExpired($key)) {
            $this->delete($key);
            return $default;
        }
        return $curr ?? $default;
    }

    public function getMultiple($keys, $default = null)
    {
        $results = [];
        foreach ($keys as $key) {
            $results[] = $this->get($key, $default);
        }
        return $results;
    }

    public function has($key)
    {
        return isset($this->items[$key]);
    }

    public function set($key, $value, $ttl = null)
    {
        $this->items[$key] = [
            'value' => $value,
            'expires' => $this->getTtl($ttl),
        ];
    }

    public function setMultiple($values, $ttl = null)
    {
        foreach ($values as $k => $v) {
            $this->set($k, $v, $ttl);
        }
    }

    protected function getTtl($ttl)
    {
        $expires = $ttl;
        if (!is_null($ttl)) {
            if (!$ttl instanceof \DateInterval) {
                $ttl = \DateInterval::createFromDateString($ttl);
            }
            $expires = (new \DateTime())->add($ttl);
        }
        return $expires;
    }

    protected function isExpired($key)
    {
        $expires = $this->items[$key]['expires'] ?? null;
        $dt = new \DateTime();
        if ($expires && $dt > $expires) {
            return true;
        }
        return false;
    }
}
