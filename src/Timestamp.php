<?php


namespace Smorken\SeqGen;


class Timestamp implements \Smorken\SeqGen\Contracts\Timestamp
{

    /**
     * @var int
     */
    protected $epoch;

    protected $format = 'Y-m-d H:i:s';

    public function __construct($epoch, $format = null)
    {
        if (!is_null($format)) {
            $this->setDateTimeFormat($format);
        }
        $this->setEpoch($epoch);
    }

    public function create(): int
    {
        return (time() - $this->getEpoch());
    }

    public function getEpoch(): int
    {
        return $this->epoch;
    }

    /**
     * @param  \DateTime|string  $dateTime
     * @throws \Smorken\SeqGen\TimestampException
     */
    public function setEpoch($dateTime): void
    {
        if (is_string($dateTime)) {
            $dateTime = $this->convertToDateTime($dateTime);
        }
        $now = new \DateTime('now');
        if ($now > $dateTime) {
            $this->epoch = $dateTime->getTimestamp();
        } else {
            throw new TimestampException("Epoch must be less than now.");
        }
    }

    public function setDateTimeFormat(string $format): void
    {
        $this->format = $format;
    }

    public function wait(): void
    {
        $current = time();
        $last = $current;
        while ($last <= $current) {
            usleep(250000);
            $last = time();
        }
    }

    protected function convertToDateTime(string $datetime): \DateTime
    {
        $dt = \DateTime::createFromFormat($this->format, $datetime);
        if ($dt !== false) {
            return $dt;
        }
        throw new TimestampException("$datetime is not in the format {$this->format}.");
    }
}
