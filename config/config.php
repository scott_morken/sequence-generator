<?php
return [
    'factory_bits' => env('SQ_FACTORY_BITS', 64),
    'default_epoch' => env('SQ_DEFAULT_EPOCH', '2020-01-01 00:00:00'),
];
