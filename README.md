## "Unique-ish" sequence generator

By default, generates a 64-bit integer based on (F64 factory):
* 32 bit timestamp (default epoch of 2020-01-01 00:00:00)
* 24 bit semi-unique identifier based on hash of client IP and optional provided integer (this is based 
on part of the hash, so it cannot be guaranteed unique)
* 8 bit rotating sequence per second per identifier (256 ids per second per identifier)

F48 factory:
* 30 bit timestamp
* 12 bit identifier
* 6 bit sequence (64 ids per second per identifier)

#### Composer

Install
```
$ composer require smorken/sequence-generator
```

#### Laravel

Should autoload when included in composer. If not, add service provider to
`config/app.php`

```php
...
    Smorken\SeqGen\ServiceProvider::class,
...
```

Get instance by DI or retrieving from the container
```php
$factory = app(\Smorken\SeqGen\Contracts\Factory::class);
$id = $factory->create('127.0.0.1');
//or with optional secondary identifier
$id = $factory->create('127.0.0.1', 12345678);
```

#### Standalone

```php
$cache = new PSR16CacheProvider();
$if = new \Smorken\SeqGen\Identifiers\Factory(new \Smorken\SeqGen\Identifiers\Ip(), new \Smorken\SeqGen\Identifiers\IntVal());
$s = new \Smorken\SeqGen\Sequence();
$t = new \Smorken\SeqGen\Timestamp('2020-01-01 00:00:00');
$factory = new \Smorken\SeqGen\Factories\F64($cache, $if, $s, $t);
$id = $factory->create('127.0.0.1');
//or with optional secondary identifier
$id = $factory->create('127.0.0.1', 12345678);
```
