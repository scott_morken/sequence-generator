<?php


namespace Tests\Smorken\SeqGen\Unit;


use PHPUnit\Framework\TestCase;
use Smorken\SeqGen\Sequence;

class SequenceTest extends TestCase
{



    public function testLastIsNullIsZero()
    {
        $sut = $this->getSut();
        $this->assertEquals(0, $sut->next(null));
    }

    public function testLastLessThanMax()
    {
        $sut = $this->getSut();
        $this->assertEquals(23, $sut->next(22));
    }

    public function testLastGreaterThanEqualMaxIsZero()
    {
        $sut = $this->getSut();
        $this->assertEquals(0, $sut->next( 255));
    }

    /**
     * @return \Smorken\SeqGen\Sequence
     */
    protected function getSut()
    {
        return new Sequence();
    }
}
