<?php


namespace Tests\Smorken\SeqGen\Unit\Identifiers;


use PHPUnit\Framework\TestCase;
use Smorken\SeqGen\Identifiers\IntVal;

class IntValTest extends TestCase
{

    public function testStringIntIsInt()
    {
        $sut = new IntVal();
        $this->assertEquals(22, $sut->create('22'));
    }

    public function testStringStartsWithIntIsInt()
    {
        $sut = new IntVal();
        $this->assertEquals(22, $sut->create('22foo'));
    }

    public function testStringWithIntIsZero()
    {
        $sut = new IntVal();
        $this->assertEquals(0, $sut->create('foo22'));
    }

    public function testObjIsOneBecauseBool()
    {
        $sut = new IntVal();
        $this->assertEquals(1, $sut->create(new \stdClass()));
    }
}
