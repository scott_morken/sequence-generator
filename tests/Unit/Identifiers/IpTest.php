<?php


namespace Tests\Smorken\SeqGen\Unit\Identifiers;


use PHPUnit\Framework\TestCase;
use Smorken\SeqGen\Identifiers\Exception;
use Smorken\SeqGen\Identifiers\Ip;

class IpTest extends TestCase
{

    public function testValidIpv4()
    {
        $sut = new Ip();
        $this->assertEquals(2130706433, $sut->create('127.0.0.1'));
    }

    public function testValidIpv6()
    {
        $sut = new Ip();
        $this->assertEquals(1, $sut->create('::1'));
    }

    public function testInvalidValidIpIsException()
    {
        $sut = new Ip();
        $this->expectException(Exception::class);
        $sut->create('1.1.1.500');
    }
}
