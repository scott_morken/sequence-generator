<?php


namespace Tests\Smorken\SeqGen\Unit;


use PHPUnit\Framework\TestCase;
use Smorken\SeqGen\Timestamp;
use Smorken\SeqGen\TimestampException;

class TimestampTest extends TestCase
{

    public function testEpochGreaterThanNowIsException()
    {
        $this->expectException(TimestampException::class);
        $this->expectExceptionMessage('Epoch must be less than now.');
        $sut = new Timestamp(date('Y-m-d H:i:s', strtotime('+1 day')));
    }

    public function testEpochWrongFormatIsException()
    {
        $this->expectException(TimestampException::class);
        $this->expectExceptionMessage('is not in the format Y-m-d H:i:s.');
        $sut = new Timestamp(date('Y-m-d H', strtotime('-1 day')));
    }

    public function testCreateIsApproximatelyCorrect()
    {
        $sut = new Timestamp(new \DateTime('-1 day'));
        $v = $sut->create();
        $this->assertEquals(86400, $v);
    }

    public function testDateTimeFormat()
    {
        $sut = new Timestamp('01/01/2020 00:00:00', 'm/d/Y H:i:s');
        $this->assertEquals(1577836800, $sut->getEpoch());
    }

    public function testWaitCanWait()
    {
        $sut = new Timestamp(new \DateTime('-1 day'));
        $v1 = $sut->create();
        $sut->wait();
        $v2 = $sut->create();
        $this->assertGreaterThan($v1, $v2);
    }
}
