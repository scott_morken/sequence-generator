<?php


namespace Tests\Smorken\SeqGen\Unit\Factories;


use PHPUnit\Framework\TestCase;
use Smorken\SeqGen\Cache\Arr;
use Smorken\SeqGen\Factories\F64;
use Smorken\SeqGen\Identifiers\Factory;
use Smorken\SeqGen\Identifiers\IntVal;
use Smorken\SeqGen\Identifiers\Ip;
use Smorken\SeqGen\Sequence;
use Smorken\SeqGen\Timestamp;

class Factory64Test extends TestCase
{

    public function testCanSplitUpId()
    {
        $expected_id = 371088911501056;
        $expected_split = [
            'timestamp' => 86400,
            'identifier' => 14598151,
            'sequence' => 0,
            'original' => 371088911501056,
        ];
        $sut = $this->getSut();
        $id = $sut->create('127.0.0.1');
        $this->assertEquals($expected_id, $id);
        $this->assertEquals($expected_split, $sut->split($id));
    }

    public function testCanSplitRealExample()
    {
        $id = 91473344087702528;
        $expected_split = [
            'timestamp' => 21297797,
            'identifier' => 9749436,
            'sequence' => 0,
            'original' => 91473344087702528,
        ];
        $sut = $this->getSut(new Timestamp('2020-01-01 00:00:00'));
        $this->assertEquals($expected_split, $sut->split($id));
    }

    public function testGetMaxTimestamp()
    {
        $sut = $this->getSut(new Timestamp('2020-01-01 00:00:00'));
        $mt = $sut->getMaxTimestamp();
        $this->assertEquals(5872804095, $mt);
    }

    public function testNoUserIdentifierIsntLikelyToRepeat()
    {
        $sut = $this->getSut();
        $vs = [];
        $ids = ['127.0.0.1', '127.0.0.2'];
        for ($i = 0; $i < 1024; $i++) {
            $v = $sut->create($ids[array_rand($ids)]);
            $this->assertArrayNotHasKey($v, $vs);
            $vs[$v] = true;
        }
    }

    public function testNoUserIdentifierRandIpIsntLikelyToRepeat()
    {
        $sut = $this->getSut();
        $vs = [];
        for ($i = 0; $i < 10240; $i++) {
            $suffix = rand(1, 254);
            $v = $sut->create('192.168.1.'.$suffix);
            $this->assertArrayNotHasKey($v, $vs);
            $vs[$v] = true;
        }
    }

    public function testWithUserIdentifierIsntLikelyToRepeat()
    {
        $sut = $this->getSut();
        $vs = [];
        $ids = [['127.0.0.1', '12'], ['127.0.0.2', 0]];
        for ($i = 0; $i < 1024; $i++) {
            $v = $sut->create(...$ids[array_rand($ids)]);
            $this->assertArrayNotHasKey($v, $vs);
            $vs[$v] = true;
        }
    }

    protected function getSut(\Smorken\SeqGen\Contracts\Timestamp $t = null)
    {
        if (is_null($t)) {
            $t = new Timestamp(date('Y-m-d H:i:s', strtotime('-1 day')));
        }
        $c = new Arr();
        $if = new Factory(new Ip(), new IntVal());
        $s = new Sequence();
        return new F64($c, $if, $s, $t);
    }
}
